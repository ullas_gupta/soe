/**
 * 
 */
package com.soe.dao;

import java.util.HashMap;
import java.util.Map;

import com.soe.model.Order;

/**
 * @author ullas
 *
 */
public interface SoeDao {

	public void InsertOrder(Order order);

}
