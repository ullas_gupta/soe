/**
 * 
 */
package com.soe.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.soe.model.Order;

/**
 * @author ullas
 *
 */
@Component
public class SoeDaoImpl implements SoeDao {

	private static final Logger logger = Logger.getLogger(SoeDaoImpl.class);

	// orderId,order
	public static Map<String, Order> orderMap = new HashMap<>();

	@Override
	public void InsertOrder(Order order) {
		logger.info("--> InsertOrder(");
		if (orderMap.get(order.getOrderId()) == null) {
			orderMap.put(order.getOrderId(), order);
		}
		logger.info("<-- InsertOrder(");
	}

	public static Map<String, Order> getOrderMap() {
		return orderMap;
	}

	public static void setOrderMap(Map<String, Order> orderMap) {
		SoeDaoImpl.orderMap = orderMap;
	}

}
