/**
 * 
 */
package com.soe.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author ullas
 *
 */
public class Order {

	@JsonProperty("StockId")
	private String orderId;
	@JsonProperty("Side")
	private String orderType;
	@JsonProperty("Company")
	private String company;
	@JsonProperty("Quantity")
	private int qty;
	
	private String orderStatus;
	private int remainingQty;
	private boolean isFulfilled = false;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public boolean isFulfilled() {
		return isFulfilled;
	}

	public void setFulfilled(boolean isFulfilled) {
		this.isFulfilled = isFulfilled;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public int getRemainingQty() {
		return remainingQty;
	}

	public void setRemainingQty(int remainingQty) {
		this.remainingQty = remainingQty;
	}

}
