package com.soe.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.soe.controller.StockOrderExchangeController;
import com.soe.dao.SoeDao;
import com.soe.dao.SoeDaoImpl;
import com.soe.model.Order;
import com.soe.orderconstatns.OrderStatusEnum;
import com.soe.orderconstatns.OrderTypeEnum;

/**
 * @author ullas
 *
 */
@Component
public class StockOrderExecutionServiceImpl implements StockOrderExecutionService {

	private static final Logger logger = Logger.getLogger(StockOrderExecutionServiceImpl.class);
	
	@Autowired
	SoeDao soeDao;

	Map<String, Order> orderMap = SoeDaoImpl.getOrderMap();

	/**
	 * 1.Take orders from orderMap and put execution result in orderMap with
	 * updated status and with remaining Qty. 2.If remaining Qty > 0 make status
	 * OPEN else CLOSED.
	 */
	@Override
	public List<Order> executeStockOrders() {
		// TODO Auto-generated method stub
		logger.info("--> executeStockOrders()");
		orderMap.forEach((k, v) -> {
			if (!v.isFulfilled()) {
				if (v.getOrderType().equalsIgnoreCase(OrderTypeEnum.BUY_ORDER.status())) {
					fulfillOrder(v, OrderTypeEnum.SELL_ORDER.status());
				} else if (v.getOrderType().equalsIgnoreCase(OrderTypeEnum.SELL_ORDER.status())) {
					fulfillOrder(v, OrderTypeEnum.BUY_ORDER.status());
				}
			}

		});

		List<Order> responseOrderList = new ArrayList<>(orderMap.values());
		logger.info("<-- executeStockOrders()");
		return responseOrderList;
	}

	/**
	 * 1.Execute the order Against all the orders of OrderMap with opposite
	 * OrderType. 2.Fulfillment is processed only against the orders in
	 * UnFulFilled status.
	 * 
	 * @param order
	 * @param searchOrderType
	 */
	protected void fulfillOrder(Order order, String searchOrderType) {
		logger.info("--> fulfillOrder()");
		orderMap.forEach((k, v) -> {
			if (v.getOrderType().equalsIgnoreCase(searchOrderType)
					&& v.getCompany().equalsIgnoreCase(order.getCompany())) {
				if (!v.isFulfilled() && !order.isFulfilled()) {
					computeFulfillment(order, v);
				}
			}
		});
		logger.info("<-- fulfillOrder()");
	}

	/**
	 * if remQty > 0 that means sellOrder is satisfied completely -- update both
	 * Orders in orderStatus Map. if remQty <0 that means buyOrder is satisfied
	 * Completely -- update both Orders in orderStatus Map.
	 * 
	 * @param buyOrder
	 * @param sellOrder
	 */
	protected void computeFulfillment(Order buyOrder, Order sellOrder) {

		logger.info("--> computeFulfillment()");
		int buyQty = buyOrder.getRemainingQty() == 0 ? buyOrder.getQty() : buyOrder.getRemainingQty();
		int sellQty = sellOrder.getRemainingQty() == 0 ? sellOrder.getQty() : sellOrder.getRemainingQty();
		int remQty = buyQty - sellQty;
		int absRemQty = Math.abs(remQty);
		// sellOrder satisfied
		if (remQty > 0) {
			sellOrder.setFulfilled(true);
			// sell order satisfied
			orderMap.get(sellOrder.getOrderId()).setOrderStatus(OrderStatusEnum.STATUS_CLOSED.status());
			orderMap.get(sellOrder.getOrderId()).setRemainingQty(0);

			// buy order in Open status
			orderMap.get(buyOrder.getOrderId()).setOrderStatus(OrderStatusEnum.STATUS_OPEN.status());
			orderMap.get(buyOrder.getOrderId()).setRemainingQty(absRemQty);
		}
		// both orders satisfied
		else if (remQty == 0) {
			buyOrder.setFulfilled(true);
			sellOrder.setFulfilled(true);

			// sellOrder satisfied
			orderMap.get(sellOrder.getOrderId()).setOrderStatus(OrderStatusEnum.STATUS_CLOSED.status());
			orderMap.get(sellOrder.getOrderId()).setRemainingQty(0);

			// buyOrder satisfied
			orderMap.get(buyOrder.getOrderId()).setOrderStatus(OrderStatusEnum.STATUS_CLOSED.status());
			orderMap.get(buyOrder.getOrderId()).setRemainingQty(0);

		}
		// buy order satisfied
		else {
			buyOrder.setFulfilled(true);

			// sellOrder in Open status
			orderMap.get(sellOrder.getOrderId()).setOrderStatus(OrderStatusEnum.STATUS_OPEN.status());
			orderMap.get(sellOrder.getOrderId()).setRemainingQty(absRemQty);

			// buyOrder satisfied
			orderMap.get(buyOrder.getOrderId()).setOrderStatus(OrderStatusEnum.STATUS_CLOSED.status());
			orderMap.get(buyOrder.getOrderId()).setRemainingQty(0);

		}
		logger.info("<-- computeFulfillment()");
	}
}
