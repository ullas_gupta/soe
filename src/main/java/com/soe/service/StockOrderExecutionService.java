package com.soe.service;

import java.util.List;

import com.soe.model.Order;

public interface StockOrderExecutionService {

	public List<Order> executeStockOrders();
}
