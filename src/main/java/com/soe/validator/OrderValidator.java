/**
 * 
 */
package com.soe.validator;

import org.springframework.stereotype.Component;

import com.soe.model.Order;
import com.soe.orderconstatns.OrderTypeEnum;

/**
 * @author ullas
 *
 */
@Component
public class OrderValidator {

	public boolean validateOrder(Order order) {
		if (order == null)
			return false;

		if (order.getOrderType().equalsIgnoreCase(OrderTypeEnum.BUY_ORDER.status())
				|| order.getOrderType().equalsIgnoreCase(OrderTypeEnum.SELL_ORDER.status())) {
			if (order.getOrderId() != null && order.getCompany() != null && order.getQty() > 0) {
				return true;
			}
		}

		return false;
	}

}
