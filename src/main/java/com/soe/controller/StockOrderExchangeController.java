/**
 * 
 */
package com.soe.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soe.dao.SoeDao;
import com.soe.model.Order;
import com.soe.service.StockOrderExecutionService;
import com.soe.util.CSVReader;
import com.soe.util.SOEUtil;
import com.soe.validator.OrderValidator;

/**
 * @author ullas
 *
 */
@RestController
@RequestMapping("/api/StockOrderExchange")
public class StockOrderExchangeController {

	private static final Logger logger = Logger.getLogger(StockOrderExchangeController.class);

	@Autowired
	SoeDao soeDao;

	@Autowired
	SOEUtil soeUtil;

	@Autowired
	StockOrderExecutionService stockOrderExecutionService;

	@Autowired
	OrderValidator soeHelper;

	@Autowired
	CSVReader csvReader;

	@RequestMapping(value = "/placeOrder", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public String placeOrder(@RequestBody Order order) {
		logger.info("--> placeOrder");
		if (soeHelper.validateOrder(order)) {
			soeDao.InsertOrder(order);
			logger.info("<-- placeOrder--save successfull");
			return soeUtil.toJSON("Saved");
		} else {
			logger.info("<-- placeOrder--save failed");
			return soeUtil.toJSON("Failed");
		}
	}

	@RequestMapping(value = "/execute", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public String execute() {
		logger.info("--> execute");
		List<Order> responseList = stockOrderExecutionService.executeStockOrders();
		logger.info("<-- execute");
		return soeUtil.toJSON(responseList);
	}

	@RequestMapping(value = "/readCsv", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public String readFromFile() {
		logger.info("--> readFromFile");
		csvReader.getInputFromFile();
		List<Order> responseList = stockOrderExecutionService.executeStockOrders();
		logger.info("<-- readFromFile");
		return soeUtil.toJSON(responseList);
	}

}
