package com.soe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoeApplication.class, args);
	}
}
