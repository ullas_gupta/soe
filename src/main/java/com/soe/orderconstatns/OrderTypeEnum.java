/**
 * 
 */
package com.soe.orderconstatns;

/**
 * @author ullas
 *
 */
public enum OrderTypeEnum {
	BUY_ORDER("Buy"), SELL_ORDER("Sell");

	private String status;

	OrderTypeEnum(String status) {
		this.status = status;
	}

	public String status() {
		return status;
	}
}
