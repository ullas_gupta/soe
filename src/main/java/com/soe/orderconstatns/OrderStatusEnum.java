package com.soe.orderconstatns;

public enum OrderStatusEnum {
	STATUS_CLOSED("Closed"), STATUS_OPEN("Open");

	private String status;

	OrderStatusEnum(String status) {
		this.status = status;
	}

	public String status() {
		return status;
	}
}
