/**
 * 
 */
package com.soe.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.soe.dao.SoeDao;
import com.soe.model.Order;
import com.soe.service.StockOrderExecutionService;
import com.soe.validator.OrderValidator;

/**
 * @author ullas
 *
 */
@Component
public class CSVReader {

	private static final Logger logger = Logger.getLogger(CSVReader.class);
	
	@Autowired
	SoeDao soeDao;

	@Autowired
	OrderValidator orderValidator;

	@Autowired
	StockOrderExecutionService stockOrderExecutionService;

	/**
	 * Get input from resource CSV and store it in the MAP.
	 * Any Csv of valid format can be used as input for this SOE system.
	 */
	public void getInputFromFile() {
		logger.info("--> getInputFile()");
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("test.csv");
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] tempStringArray = line.substring(line.indexOf(",") + 1).split(",");

				String stockId = line.substring(0, line.indexOf(cvsSplitBy));
				String side = tempStringArray[0];
				String company = tempStringArray[1];
				long quantity = Long.parseLong(tempStringArray[2]);

				Order orderObj = new Order();
				orderObj.setOrderType(side);
				orderObj.setCompany(company);
				orderObj.setQty((int) quantity);
				orderObj.setOrderId(stockId);

				if (orderValidator.validateOrder(orderObj)) {
					soeDao.InsertOrder(orderObj);
				}
			}

		} catch (FileNotFoundException e) {
			logger.info("--! error @getInputFile()");
			e.printStackTrace();
		} catch (IOException e) {
			logger.info("--! error @getInputFile()");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			logger.info("<-- getInputFile()");
		}
	}
}
