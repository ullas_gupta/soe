/**
 * 
 */
package com.soe.service;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.soe.dao.SoeDaoImpl;
import com.soe.util.CSVReader;

/**
 * @author ullas
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StockOrderExecutionServiceTest {

	// A test to load the input stock orders from input.csv and execute and
	// print the output.
	@Spy
    @InjectMocks
	StockOrderExecutionServiceImpl stockOrderExecutionServiceImpl;

	@Autowired
	CSVReader csvReader;

	/**
	 * Test to load orders from an input CSV into the map and assert the
	 * expected output.
	 */
	@Test
	public void executeOrdersTest() {
		// loads the input from file
		csvReader.getInputFromFile();
		// exectue orders.
		stockOrderExecutionServiceImpl.executeStockOrders();

		assertThat(SoeDaoImpl.getOrderMap().values(), is(5));

	}

}
