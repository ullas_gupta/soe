# README #

This is a basic implementation of StockOrderExchange System.
The system accepts input in the form of a CSV or from a REST endpoint.

### How do I get set up? ###

Clone the repo as it is and import the springBoot project.
Recommended IDE - Spring Tool Suite.(Since it is a springBoot app and was created using the same.)

### How to Run and test? ###

1.Bring up the spring boot application from your IDE.

2.Testing can be either done Manually or Using a CSV file.

3.Manual testing/Usage - : 
      
      a. Hit the Service "/api/StockOrderExchange" eg. 
         "http://localhost:8080/api/StockOrderExchange/placeOrder"(Post)
          with Body :
        {
	"StockId":"5",
	"Side":"Buy",
	"Company":"XYZ",
	"Quantity":8
        }
       NOTE - stockId should be unique.

      b. Once required number of buy and sell orders are placed in the system we call the service to 
         execute the orders.
         "http://localhost:8080/api/StockOrderExchange/execute"(GET)
         This service processes the orders and returns the List of processed orders with their 
         fulfillment status and currentOrder status.

4.Test using CSV file Input :

      a. CSV file provided in src/main/resources folder test.csv

      b. The System will accept the file with the valid format as the test.csv.

      c. Hit the Service "http://localhost:8080/api/StockOrderExchange/readCsv"
         to read from Csv file provided and the system will process the Orders in the CSV file and 
         return the valid outPut. 

      d. To change the file simply replace the test.csv file in src/main/resources and run the service 
         again.(May require a server restart)

     
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact